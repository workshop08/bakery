import Bakery.Bakery;
import Bakery.Customer;
import Bakery.Flavour;
import View.BakeryGraphics;
import View.GraphicsExample;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Main extends Application{

    public void start(Stage primaryStage){
        Flavour[] flavours = Flavour.values();
        Bakery bakery = new Bakery(flavours, 5, 5000);
      //  Customer customer = new Customer(2000, bakery);
        bakery.startBakery();
      //  customer.start();
        BakeryGraphics bakeryGraphics = new BakeryGraphics(bakery);

        bakeryGraphics.start(primaryStage);
    }

    public static void main(String[] args) {
        launch();
    }
}
