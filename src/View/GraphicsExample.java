package View;

import javafx.animation.RotateTransition;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.effect.Glow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

public class GraphicsExample extends Application {
    @Override
    public void start(Stage primaryStage){
        Group root = new Group();
        Scene scene = new Scene(root, 1200, 600);
        primaryStage.setTitle("Hello there");
        primaryStage.setScene(scene);

        Line line = new Line(1, 1, 500,500);
        ObservableList rootChildren = root.getChildren();
        rootChildren.add(line);

        Path path = new Path(new MoveTo(10, 100), new LineTo(100, 100), new LineTo(200, 250));
        rootChildren.add(path);

        Text text = new Text(50, 50, "а)");
        text.setFont(Font.font("arial", 40));
        text.setFill(Color.GREEN);
        rootChildren.add(text);

        Image image = new Image("https://i.ytimg.com/vi/4i1-4hzDXTo/maxresdefault.jpg");
        ImageView imageView = new ImageView(image);
        imageView.fitHeightProperty();
        imageView.setPreserveRatio(true);
        rootChildren.add(imageView);

        RotateTransition rotateTransition = new RotateTransition();
        rotateTransition.setDuration(Duration.millis(1000));
        rotateTransition.setNode(imageView);
        rotateTransition.setByAngle(360);
        rotateTransition.setCycleCount(10);
        rotateTransition.play();

        primaryStage.show();
    }
}
