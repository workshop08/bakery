package View;

import Bakery.Bakery;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import static javafx.scene.input.KeyCode.ACCEPT;

public class BakeryGraphics{
    private static final String LineCookiesSold = "Cookies sold: ";
    private Bakery bakery;

    public BakeryGraphics(Bakery bakery) {
        this.bakery = bakery;
    }

    public void start(Stage primaryStage){
        Group root = new Group();
        Scene scene = new Scene(root, 600, 600);
        primaryStage.setTitle("Bakery");
        primaryStage.setScene(scene);

        primaryStage.setOnCloseRequest(e -> handleExit());
        Button buyCookieButton = new Button("I'm a Button");
        buyCookieButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                EventType<ActionEvent> eventType = (EventType<ActionEvent>) actionEvent.getEventType();
                System.out.println(eventType);
                bakery.sellCookie();
            }
        });
        root.getChildren().add(buyCookieButton);
        EventHandler eventHandler = new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                switch (keyEvent.getCode()){
                    case ENTER: bakery.sellCookie(); break;
                    default: break;
                }
            }
        };
        scene.addEventHandler(KeyEvent.KEY_PRESSED, eventHandler);

        Text text = new Text(50, 50, LineCookiesSold + bakery.getCookiesSold());
        text.setFont(Font.font(20));
        bakery.addPropertyChangeListener(Bakery.SOLD_COOKIES, pcEvent -> updateSoldCookies(text, bakery.getCookiesSold()));

        root.getChildren().add(text);
        primaryStage.show();
    }

    private void handleExit() {
        Platform.exit();
        System.exit(0);
    }

    private void updateSoldCookies(Text text, int cookiesSold)
    {
        text.setText(LineCookiesSold + cookiesSold);
    }
}
