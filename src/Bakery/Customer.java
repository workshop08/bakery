package Bakery;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Customer extends Thread {
    private long eatingTime;
    private Bakery bakery;
    Logger logger;

    public Customer(long eatingTime, Bakery bakery)
    {
        logger = Logger.getLogger(Customer.class.getName());
        logger.setLevel(Level.INFO);
        this.bakery = bakery;
        this.eatingTime = eatingTime;
    }

    public void run() {
        while (!this.isInterrupted()) {
            Cookie newCookie = bakery.sellCookie();
            logger.info(newCookie.getFlavour().name);

                synchronized (this){
                    try {
                        wait(eatingTime);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
        }
    }
}
