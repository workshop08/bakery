package Bakery;

import javax.swing.event.SwingPropertyChangeSupport;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

public class Bakery {
    public static final String SOLD_COOKIES = "cookies_sold";
    private CookieStorage cookieStorage;
    private ArrayList<Baker> bakers;
    private int cookiesSold;
    private SwingPropertyChangeSupport pcSupport = new SwingPropertyChangeSupport(this);

    public Bakery(Flavour[] flavours, int storageCapacity, long cookingTime){
        cookieStorage = new CookieStorage(storageCapacity);
        bakers = new ArrayList<>();
        for (Flavour flavour:flavours) {
            bakers.add(new Baker(flavour, cookingTime, cookieStorage));
        }
        cookiesSold = 0;
    }

    public void startBakery() {
        for (Baker baker: bakers) {
            baker.start();
        }
    }

    public Cookie sellCookie(){
        Cookie soldCookie = cookieStorage.getCookie();
        int oldValue = cookiesSold;
        cookiesSold++;
        int newValue = cookiesSold;
        pcSupport.firePropertyChange(SOLD_COOKIES, oldValue, newValue);
        return soldCookie;
    }

    public int getCookiesSold() {
        return cookiesSold;
    }

    public void addPropertyChangeListener(String name, PropertyChangeListener listener) {
        pcSupport.addPropertyChangeListener(name, listener);
    }
}
