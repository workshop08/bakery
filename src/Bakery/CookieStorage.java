package Bakery;

import java.util.Stack;

public class CookieStorage {
    Stack<Cookie> cookies;
    int capacity;

    public CookieStorage(int capacity) {
        this.cookies = new Stack<>();
        this.capacity = capacity;
    }

    public void addCookie(Cookie cookie){
        synchronized (cookies) {
            while (cookies.size() >= capacity) {
                try {
                    cookies.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            cookies.add(cookie);
            cookies.notify();
        }
    }

    public Cookie getCookie(){
        Cookie soldCookie;
        synchronized (cookies) {
            while (cookies.isEmpty()) {
                try {
                    cookies.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            soldCookie = cookies.pop();
            cookies.notify();
        }
        return soldCookie;
    }

}
