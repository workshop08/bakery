package Bakery;

public enum Flavour {
    STRAWBERRY ("Strawberry"),
    VANILLA ("Vanilla"),
    CHOCOLATE("Chocolate");

    String name;
    Flavour(String name){
        this.name = name;
    }
}
