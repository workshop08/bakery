package Bakery;

public class Cookie {
    Flavour flavour;

    public Cookie(Flavour flavour) {
        this.flavour = flavour;
    }

    public Flavour getFlavour() {
        return flavour;
    }
}
