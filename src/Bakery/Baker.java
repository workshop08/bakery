package Bakery;

public class Baker extends Thread{
    private long cookingTime;
    CookieStorage cookieStorage;
    Flavour flavour;

    public Baker(Flavour flavour, long cookingTime, CookieStorage cookieStorage){
        this.flavour = flavour;
        this.cookieStorage = cookieStorage;
        this.cookingTime = cookingTime;
    }

    public void run(){
        while (!this.isInterrupted()) {
            synchronized (this) {
                try {

                    wait(cookingTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            synchronized (cookieStorage) {
                cookieStorage.addCookie(new Cookie(flavour));
            }
        }
    }
}
